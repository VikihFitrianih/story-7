from django.test import TestCase
from django.test import Client

# Create your tests here.
class UnitTest(TestCase):
    def test_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_url_not_exist(self):
        response = Client().get('/oi')
        self.assertEqual(response.status_code, 404)

    def test_page(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_index_contains_greeting(self):
        response = self.client.get('/')
        response_content = response.content.decode('utf-8')
        self.assertIn("My Profile", response_content)